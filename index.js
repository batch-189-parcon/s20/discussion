// alert('hello');

/*
	While Loop

		Syntax:
			While(expression/condition) {
					statement
			}

*/


let count = 5

while (count !== 0) {
	console.log("While: " + count)
	count-- //count -= 1
}


let x = 1

while (x <= 5) {
	console.log(x)
	x++;
}



/*
	DO WHILE LOOP
		A do-while loop works a lot like the while loop. But unlike
		while loops, do-while loops guarantee that the code will be executed
		at least once.

	Syntax:
		do {
			statement
		} while (expression/condition)

*/

// let number = Number(prompt("Give me a number: "))

// do {

// 	console.log("Do While: " + number)
// 	// number = number + 1
// 	number += 1
// } while (number < 10)


/*
	FOR LOOP
		- more flexible than while loop and do-while loop

	Syntax:
		for (initialization, expression/condition; finalExpression) {
			statement
		}

*/

for(let count = 0; count <= 20; count++) {
	console.log('For Loop: ' + count)
}

let myString = "Enteng Kabisote"
console.log(myString.length)

console.log(myString[0])
console.log(myString[1])
console.log(myString[14])

for(let x = 0; x < myString.length; x++){
	console.log(myString[x])
}

let myName = "TRISTAN";

for(let i = 0; i < myName.length; i++) {

	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
	) {
		console.log('Vowel')
	} else {
		console.log(myName[i])
	}
}

// Continue and Break Statements
/*
	"Continue" statement allows the code to go to the next iteration
	of the loop without finishing the execution of all statements
	in a code block

	"Break" statement is used to terminate the current loop once a match
	has been found.
*/

for (let count = 0; count <= 20; count++) {

	console.log('Hello World ' + count)

	if(count % 2 === 0){
		console.log('Even number ')
		continue;
	}

	console.log('Continue and Break ' + count)

	if(count > 10) {
		break;
	}
}


let name = "Alexandro"

for(let i = 0; i < name.length; i++) {
	console.log(name[i]);

	if(name[i].toLowerCase() === "a") {
		console.log('Continue to the next iteration')
		continue;
	}

	console.log('Hello')

	if(name[i].toLowerCase() === "d") {
		break;
	}
}